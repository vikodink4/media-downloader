from api.instagram import InstagramDownloadAPI


def download_profile_instagram_content(profile: str, max_count: int, datatype: str):
    api = InstagramDownloadAPI(profile, datatype)
    posts = api.get_image_url_list(max_count=max_count)
    print('\nDownloading instagram metadata for profile {} has started\n'.format(profile))
    api.download_image(url_list=posts)
    api.save_image_url(url_list=posts)
    print('\nDownloading has ended\n'.format(profile))


def download_tag_instagram_content(hashtag: str, max_count: int, datatype: str):
    api = InstagramDownloadAPI(hashtag, datatype)
    hashtag_posts = api.get_image_list_by_tag(max_count)
    print('\nDownloading instagram metadata for hashtag #{} has started\n'.format(hashtag))
    api.download_image(url_list=hashtag_posts)
    api.save_image_url(url_list=hashtag_posts)
    print('\nDownloading has ended\n'.format(hashtag))
