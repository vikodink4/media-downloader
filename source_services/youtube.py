from api.youtube import YoutubeDownloadAPI


def download_youtube_channel_content(profile: str, max_count: int, datatype: str):
    print('Youtube downloader has started')
    api = YoutubeDownloadAPI(profile, datatype)
    video_list = api.get_video_id_list(max_count)
    print('\nDownloading metadata for youtube channel {} has started\n'.format(profile))
    api.save_videos_metadata(video_list)
    print('\nDownloading has ended\n'.format(profile))


def download_youtube_video_content(video_id: str, datatype: str):
    print('Youtube downloader has started')
    api = YoutubeDownloadAPI(video_id, datatype)
    print('\nDownloading metadata for video with id {} has started\n'.format(video_id))
    api.save_video_metadata(video_id)
    print('\nDownloading has ended\n'.format(video_id))