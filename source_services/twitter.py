from api.twitter import TwitterDownloadApi


def download_profile_twitter_content(identifier, max_count, datatype):
    print('Twitter downloader has started')
    api = TwitterDownloadApi(datatype, max_count, identifier)
    print('Getting urls for media in tweets')
    tweets_media_urls = api.get_tweets_media_urls_from_profile()
    print('Downloading data for profile {} has started'.format(identifier))
    api.save_tweets_media_data(tweets_media_urls)
    print('Downloading has ended')


def download_tag_twitter_content(identifier, max_count, datatype):
    print('Twitter downloader has started')
    api = TwitterDownloadApi(datatype, max_count, identifier)
    print('Getting urls for media in tweets')
    tweets_media_urls = api.get_tweets_media_urls_from_search_by_tag()
    print('Downloading data for search query {} has started'.format(identifier))
    api.save_tweets_media_data(tweets_media_urls)
    print('Downloading has ended')
