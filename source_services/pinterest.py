from api.pinterest import PinterestDownloadAPI


def download_search_pinterest_content(identifier: str, max_count: int, datatype: str):
    print('Fetching pinterest data for pin search query #{} has started\n'.format(identifier))
    print('THE BROWSER WILL BE OPENED IN A FEW SECONDS...')
    PinterestDownloadAPI.wait(3)
    api = PinterestDownloadAPI(datatype, max_count, identifier)
    print('Authorizing\n')
    api.authorize()
    print('Getting pins metadata to downloading\n')
    api.get_pins_from_search()
    pin_list = api.get_pin_list()
    print('Downloading posts url\n')
    api.save_posts_url(pin_list)
    print('Downloading pins images (it may take several minutes)\n')
    api.save_pins_images(pin_list)
    print('Downloading has ended\n')
    api.browser.execute_script('window.alert("DOWNLOADING HAS ENDED.")')
    api.wait(2)
    api.browser.quit()


def download_profile_pinterest_content(identifier: str, max_count: int, datatype: str):
    print('Fetching pinterest data for profile {} has started\n'.format(identifier))
    print('THE BROWSER WILL BE OPENED IN A FEW SECONDS...')
    PinterestDownloadAPI.wait(3)
    api = PinterestDownloadAPI(datatype, max_count, identifier)
    print('Authorizing\n')
    api.authorize()
    print('Getting pins metadata to downloading\n')
    api.get_pins_from_profile()
    pin_list = api.get_pin_list()
    print('Downloading posts url\n')
    api.save_posts_url(pin_list)
    print('Downloading pins images (it may take several minutes)\n')
    api.save_pins_images(pin_list)
    print('Downloading has ended\n')
    api.browser.execute_script('window.alert("DOWNLOADING HAS ENDED.")')
    api.wait(2)
    api.browser.quit()

