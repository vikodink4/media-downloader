HOW TO USE THE SCRIPT

### PREPARING ###

1. Create virtual environment in the project directory:
	$ virtualenv -p python3 venv

2. Activate it:
	$ source venv/bin/activate

3. Install dependencies:
	$ pip install -r requirements.txt


### RUNNING ###

Using of the script based on the terminal command which contains:
	 - python3 (version of python interpreter which should run the script);
	 - social_scrapper.py (the script name);
	 - youtube / instagram / pinterest (source datatype);
	 - -i <video_id / channel_id / profile_id / hashtag / search_query> (search identifier for each social media);
	 - --max-count <number> (count of downloaded data).

Examples:

1. Youtube:
	- video_id:
		$ python3 social_scrapper.py youtube video -i l9o33uBi40U
		(l9o33uBi40U - video_id)
		(here --max-count doesn't needed because a video is single)

	- channel_id:
		$ python3 social_scrapper.py youtube channel -i UC3n0qf54OPWei0bVF4W60Gw --max-count 100
		(UC3n0qf54OPWei0bVF4W60Gw - channel_id)

2. Instagram:
	- profile:
		$ python3 social_scrapper.py instagram profile -i positiveminds11 --max-count 100
		(positive_minds11 - profile_id)

	- hashtag:
        $ python3 social_scrapper.py instagram hashtag -i successquotes --max-count 100
        (cats - hashtag)

3. Pinterest:
	- search pins:
		$ python3 social_scrapper.py pinterest search_pins -i wiser%20quotes --max-count 100
		(cats - pins search query, 100 - max count that pinterest allows to fetch)

    - profile pins:
        $ python3 social_scrapper.py pinterest profile -i TheFunnyBeaver --max-count 100

4. Twitter:
    - profile media from tweets:
        $ python3 social_scrapper.py twitter profile -i SpaceX --max-count 100
        (SpaceX - profile)

    - media from tweets from search by tag:
        $ python3 social_scrapper.py twitter tag -i "ElonMusk" --max-count 100
        ("ElonMusk" - tag)

!!! WARNINGS !!!
TWITTER: if you face with an error "The request limit to Twitter has reached out. Please try again your request after 15 minutes",
it means currently you're unable to do new requests to Twitter for further 15 mins (Twitter restrictions), read more:
https://developer.twitter.com/en/docs/basics/rate-limiting

PINTEREST: Pinterest media downloader requires using chrome-driver. It means chrome browser will be opened while pinterest media downloader is running. Highly recommended to not move or not open browser window.
