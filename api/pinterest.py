import os
import time
import urllib.request as urllib

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from api.mixins.download_mixins import DownloadMixin
from common import CHROMEDRIVER_PATH, PINTEREST_EMAIL, PINTEREST_PASSWORD


class PinterestDownloadAPI(DownloadMixin):
    def __init__(self, datatype: str, pins_count: int, identifier: str):
        self.datatype = datatype
        self.option = webdriver.ChromeOptions().add_argument(' - incognito')
        self.browser = webdriver.Chrome(executable_path=CHROMEDRIVER_PATH, chrome_options=self.option)
        self.pins_count = pins_count
        self.identifier = identifier
        self.pinterest_url = 'https://in.pinterest.com/'
        self.pinterest_search_url = 'https://in.pinterest.com/search/pins/?q={}'
        self.pinterest_login_page = 'https://in.pinterest.com/login/?referrer=home_page'

    def open_url(self, link: str):
        self.browser.set_window_size(1200, 800)
        self.browser.get(link)
        self.wait(2)

    @staticmethod
    def wait(seconds: int):
        time.sleep(seconds)

    def scroll_page_down(self, start_pos: int, end_pos: int):
        self.browser.execute_script("window.scrollTo({}, {})".format(start_pos, end_pos))
        self.wait(3)

    def authorize(self, email: str=PINTEREST_EMAIL, password: str=PINTEREST_PASSWORD):
        self.open_url(self.pinterest_login_page)
        login_input = self.browser.find_element_by_xpath('//input[@name="id"]')
        login_input.send_keys(email)
        self.wait(2)
        password_input = self.browser.find_element_by_xpath('//input[@name="password"]')
        password_input.send_keys(password)
        self.wait(1)
        password_input.send_keys(Keys.ENTER)

    def get_pins_from_profile(self):
        self.wait(1)
        self.open_url(self.pinterest_url + self.identifier)
        self.wait(2)
        self.scroll_page_down(0, 1000)
        self.open_url(self.pinterest_url + self.identifier + '/pins')
        # see_all_pins_button = self.browser.find_element_by_xpath('//button[@aria-label="See all"]')
        # print(see_all_pins_button)
        # see_all_pins_button.click()
        self.wait(2)

    def get_pins_from_search(self):
        self.wait(1)
        self.open_url(self.pinterest_search_url.format(self.identifier))
        self.wait(1)

    def get_pin_list(self):
        pin_list = []
        current_position_Y = 500

        while len(pin_list) < self.pins_count:
            pins = self.browser.find_elements_by_xpath('//div[@class="pinWrapper"]')
            self.wait(2)

            for index, pin in enumerate(pins):
                pin_link = pin.find_element_by_tag_name('a')
                post_url = pin_link.get_attribute('href')

                try:
                    img = pin.find_element_by_tag_name('img')
                except:
                    continue

                img_src_link = img.get_attribute('srcset').split(',')[-1].replace(' 4x', '')

                if img_src_link not in [link.get('post_url') for link in pin_list]:
                    if len(pin_list) < self.pins_count:
                        pin_metadata = {'post_url': post_url, 'img_src_link': img_src_link}
                        pin_list.append(pin_metadata)
                        continue
                    else:
                        break

            current_position_Y += 3500
            self.scroll_page_down(0, current_position_Y)
            self.wait(3)

        return pin_list

    def save_posts_url(self, pin_list: list):
        for pin in pin_list:
            post_url = pin.get('post_url')
            post_url_id = post_url.split('/')[-2]

            filepath = self.get_download_directory(post_url_id, datatype=self.datatype,
                                                   source_name='pinterest', identifier=self.identifier)

            with open(filepath + '_POST_URL.txt', 'w') as f:
                f.write(post_url)
                f.close()

    def save_pins_images(self, pin_list: list):
        for pin in pin_list:
            post_url = pin.get('post_url')
            post_url_id = post_url.split('/')[-2]
            image_src_link = pin.get('img_src_link')

            filepath = self.get_download_directory(post_url_id, datatype=self.datatype,
                                                   source_name='pinterest', identifier=self.identifier)

            urllib.urlretrieve(image_src_link, filepath + '.jpg')
