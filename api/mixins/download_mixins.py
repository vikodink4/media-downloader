import os

from common import ROOT_DIR


class DownloadMixin:
    def get_download_directory(self, filename: str, source_name: str, identifier: str, datatype: str):
        """
        Returns download directory for each entity (image or url).
        Download folder structure:
            - downloads:
                - <source>:
                    - <profile_name>:
                        - <last_part_of_post_url>(folder):
                            - <last_part_of_post_url_image>(image file)
                            - <last_part_of_post_url_url>(text file with url)

        :param filename: name of file.
        :return: directory for downloading current file (image or url).
        """

        main_download_folder = ROOT_DIR + '/downloads'

        if not os.path.exists(main_download_folder):
            os.makedirs(main_download_folder)

        source_folder = main_download_folder + '/' + source_name

        if not os.path.exists(source_folder):
            os.makedirs(source_folder)

        datatype_folder = source_folder + '/' + datatype

        if not os.path.exists(datatype_folder):
            os.makedirs(datatype_folder)

        current_profile_download_folder = datatype_folder + '/' + identifier

        if not os.path.exists(current_profile_download_folder):
            os.makedirs(current_profile_download_folder)

        current_post_download_folder = current_profile_download_folder + '/' + filename

        if not os.path.exists(current_post_download_folder):
            os.makedirs(current_post_download_folder)

        return current_post_download_folder + '/' + filename