import tweepy
import urllib.request as urllib

from tweepy.error import TweepError
from api.mixins.download_mixins import DownloadMixin
from common import TWITTER_ACCESS_TOKEN, TWITTER_ACCESS_TOKEN_SECRET, TWITTER_CONSUMER_KEY, TWITTER_CONSUMER_SECRET


class TwitterDownloadApi(DownloadMixin):
    def __init__(self, datatype: str, max_count: int, identifier: str):
        auth = tweepy.OAuthHandler(TWITTER_CONSUMER_KEY, TWITTER_CONSUMER_SECRET)
        auth.set_access_token(TWITTER_ACCESS_TOKEN, TWITTER_ACCESS_TOKEN_SECRET)

        self.api = tweepy.API(auth)
        self.datatype = datatype
        self.max_count = max_count
        self.identifier = identifier

    def get_tweets_media_urls_from_profile(self):
        media_urls = []

        try:
            for tweet in tweepy.Cursor(self.api.user_timeline, id='@' + str(self.identifier)).items():
                if len(media_urls) < self.max_count:
                    tweet_container = self.get_tweet_media_url(tweet)

                    if tweet_container is not None:
                        media_urls.append(tweet_container)
                else:
                    break
        except TweepError as e:
            if '429' in str(e):
                raise Exception('\nThe request limit to Twitter has reached out. Please try again your request after 15 minutes')

        return media_urls

    def get_tweets_media_urls_from_search_by_tag(self):
        media_urls = []

        try:
            for tweet in tweepy.Cursor(self.api.search, q='#' + self.identifier).items():
                if len(media_urls) < self.max_count:
                    tweet_container = self.get_tweet_media_url(tweet)

                    if tweet_container is not None:
                        media_urls.append(tweet_container)
                else:
                    break
        except TweepError as e:
            if '429' in str(e):
                raise Exception('\nThe request limit to Twitter has reached out. Please try again your request after 15 minutes')

        return media_urls

    def get_tweet_media_url(self, tweet):
        try:
            extended_entities = tweet.extended_entities
            media_data_type = tweet.extended_entities.get('media')[0].get('type')

            if media_data_type == 'photo':
                media_url = extended_entities.get('media')[0].get('media_url')
                tweet_id = tweet.id
                tweet_container = {'media_url': media_url, 'tweet_id': tweet_id}

                return tweet_container
            elif media_data_type == 'video':
                video_variants = extended_entities.get('media')[0].get('video_info').get('variants')
                try:
                    media_url = [url_dict for url_dict in video_variants if url_dict.get('content_type')
                                 == 'video/mp4' and url_dict.get('bitrate') > 800000][0].get('url')
                except Exception as e:
                    media_url = [url_dict for url_dict in video_variants if url_dict.get('content_type')
                                 == 'video/mp4' and url_dict.get('bitrate') > 200000][0].get('url')

                tweet_id = tweet.id
                tweet_container = {'media_url': media_url, 'tweet_id': tweet_id}

                return tweet_container
        except AttributeError:
            pass

    def save_tweets_media_data(self, tweet_containers):
        for tweet_container in tweet_containers:
            media_url = tweet_container.get('media_url')
            tweet_id = str(tweet_container.get('tweet_id'))

            filepath = self.get_download_directory(tweet_id, datatype=self.datatype,
                                                   source_name='twitter', identifier=self.identifier)

            ext = '.jpg' if 'jpg' in media_url else '.mp4'
            urllib.urlretrieve(media_url, filepath + ext)
