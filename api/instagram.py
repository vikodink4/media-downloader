import datetime
import instaloader

from api.mixins.download_mixins import DownloadMixin


class InstagramDownloadAPI(DownloadMixin):
    """
    Service allows downloading images and its URL from the Instagram.
    """

    def __init__(self, identifier: str, datatype: str, context=instaloader.InstaloaderContext(), loader=instaloader.Instaloader()):
        """
        :param identifier: (user) name of profile from which you want to download content OR tag.
        :param context: instaloader (lib) context.
        :param loader: object of instaloader (lib).
        :param datatype: source datatype (`profile` or `hashtag`)
        """

        self.identifier = identifier
        self.context = context
        self.loader = loader
        self.datatype = datatype

    def get_image_url_list(self, max_count: int=100) -> list:
        """
        Creates and returns a list with profile's posts (image) urls.

        :param max_count: max count of downloaded content (url)
        :return: list of image urls in str.
        """

        profile = instaloader.Profile(context=self.context, node={'username': self.identifier})
        posts = profile.get_posts()
        posts_url = []

        for post in posts:
            if len(posts_url) < max_count:
                if not post.is_video:
                    posts_url.append(post.url)
                else:
                    continue
            else:
                break

        return posts_url

    def get_image_list_by_tag(self, max_count: int) -> list:
        """
        Gets, creates and returns list of post (image) data searched by hashtag.

        :param max_count: max count of posts which be returned in list.
        :return: a list of post objects.
        """

        hashtag = self.identifier
        loader = self.loader
        posts_by_hashtag = loader.get_hashtag_posts(hashtag)

        hashtag_posts = []

        for post in posts_by_hashtag:
            if len(hashtag_posts) < max_count:
                if not post.is_video:
                    hashtag_posts.append(post.url)
                else:
                    continue
            else:
                break

        return hashtag_posts

    def download_image(self, filepath: str = None, url: str = None, time: datetime.datetime=datetime.datetime.now(),
                       url_list: list=None):
        """
        Downloads images to its directory.

        :param filepath: path (folder) where file should be saved.
        :param url: post (image) url in Instagram.
        :param time: timestamp.
        :param url_list: list of post (image) urls that should be saved.
        """

        if url_list is None and url:
            self.loader.download_pic(filepath, url, time)

        if url_list:
            for url in url_list:
                name_from_url = url.split('/')[-1].split('.jpg')[0]
                filepath = self.get_download_directory(name_from_url,
                                                       datatype=self.datatype,
                                                       source_name='instagram', identifier=self.identifier)
                self.loader.download_pic(filepath, url, time)

    def save_image_url(self, url_list: list = None):
        """
        Saves urls in the txt file.

        :param url_list: list with urls.
        """

        if url_list:
            for url in url_list:
                name_from_url = url.split('/')[-1].split('.jpg')[0]
                filepath = self.get_download_directory(name_from_url, datatype=self.datatype,
                                                       source_name='instagram', identifier=self.identifier)

                with open(filepath + '_url.txt', 'w') as f:
                    f.write(url)
                    f.close()


