import requests
import urllib.request as urllib

from common import YOUTUBE_DEVELOPER_KEY
from api.mixins.download_mixins import DownloadMixin


class YoutubeDownloadAPI(DownloadMixin):
    """
    Service allows downloading video data from the certain channel ID
    (video title, video description, video thumbnail, video url).
    """

    def __init__(self, identifier: str, datatype: str):
        """
        :param identifier: specific video ID OR channel ID.
        :param datatype: source datatype (`channel` or `video`)
        """

        self.identifier = identifier
        self.datatype = datatype

    def get_video_id_list(self, max_count: int) -> list:
        """
        :param max_count: max count of videos to downloading data.
        :return: list with IDs of videos.
        """

        id_list = []
        next_token = None

        while len(id_list) < max_count:
            if not next_token:
                link = 'https://www.googleapis.com/youtube/v3/search?part=id' \
                       '&channelId={}&type=video&maxResults=1&key={}'.format(self.identifier,
                                                                              YOUTUBE_DEVELOPER_KEY)
            else:
                link = 'https://www.googleapis.com/youtube/v3/search?pageToken={}&part=id' \
                       '&channelId={}&type=video&maxResults=1&key={}'.format(next_token,
                                                                              self.identifier,
                                                                              YOUTUBE_DEVELOPER_KEY)
            response_from_api = requests.request('get', link)
            jsoned_response_from_api = response_from_api.json()
            next_token = jsoned_response_from_api.get('nextPageToken')
            channel_data = jsoned_response_from_api.get('items')

            for index, item in enumerate(channel_data):
                video_id = item.get('id')['videoId']
                id_list.append(video_id)

        return id_list

    def save_video_metadata(self, video_id: str):
        """
        Gets and downloads metadata (title, description, thumbnail, url) for specific video ID.

        :param video_id: YouTube video ID
        """

        link = 'https://www.googleapis.com/youtube/v3/videos?' \
               'id={}&part=snippet%2CcontentDetails%2Cstatistics&key={}'.format(video_id,
                                                                                YOUTUBE_DEVELOPER_KEY)
        response_from_api = requests.request('get', link)
        jsoned_response_from_api = response_from_api.json()

        video_description = jsoned_response_from_api.get('items')[0].get('snippet').get('localized').get('description')
        video_title = jsoned_response_from_api.get('items')[0].get('snippet').get('localized').get('title').replace('/', '|')
        video_hq_thumbnail_url = jsoned_response_from_api.get('items')[0].get('snippet').get('thumbnails').get(
            'high').get('url')
        video_url = 'https://www.youtube.com/watch?v={}'.format(jsoned_response_from_api.get('items')[0].get('id'))

        self.download_video_description(video_description, video_title)
        self.download_video_title(video_title)
        self.download_video_thumbnail(video_hq_thumbnail_url, video_title)
        self.download_video_url(video_url, video_title)

    def save_videos_metadata(self, video_id_list: list):
        """
        Gets and downloads metadata (title, description, thumbnail, url) for list with its ids.
        The method passes video metadata received from YouTube API to another methods
        which saves every metadata type separately for each video ID.

        :param video_id_list: list with IDs of videos.
        """

        for video_id in video_id_list:
            self.save_video_metadata(video_id)

    def download_video_description(self, video_description: str, video_title: str):
        """
        Saves video description in .txt for video ID in its download directory.

        :param video_description: video description from the YouTube API
        :param video_title: video description from the YouTube API
        """

        filepath = self.get_download_directory(video_title, datatype=self.datatype,
                                               source_name='youtube', identifier=self.identifier)

        with open(filepath + '_DESCRIPTION.txt', 'w') as f:
            f.write(video_description)
            f.close()

    def download_video_title(self, video_title: str):
        """
        Saves video title in .txt for video ID in its download directory.

        :param video_title: video title from the YouTube API
        """

        filepath = self.get_download_directory(video_title, source_name='youtube', identifier=self.identifier,
                                               datatype=self.datatype)

        with open(filepath + '_TITLE.txt', 'w') as f:
            f.write(video_title)
            f.close()

    def download_video_thumbnail(self, video_thumbnail_url: str, video_title: str):
        """
        Saves video thumbnail in .jpg for video ID in its download directory.

        :param video_thumbnail_url: video thumbnail URL from the YouTube API
        :param video_title: video title from the YouTube API
        """

        filepath = self.get_download_directory(video_title, source_name='youtube', identifier=self.identifier,
                                               datatype=self.datatype)
        urllib.urlretrieve(video_thumbnail_url, filepath + '.jpg')

    def download_video_url(self, video_url: str, video_title: str):
        """
        Saves video URL in .txt for video ID in its download directory.

        :param video_url: video URL from the YouTube API
        :param video_title: video title from the YouTube API
        """
        filepath = self.get_download_directory(video_title, source_name='youtube', identifier=self.identifier,
                                               datatype=self.datatype)

        with open(filepath + '_URL.txt', 'w') as f:
            f.write(video_url)
            f.close()

