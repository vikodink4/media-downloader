#TODO:
"""
Pinterest
To scrape the images, post url
"""

import argparse

from source_services.youtube import download_youtube_channel_content, download_youtube_video_content
from source_services.instagram import download_profile_instagram_content, download_tag_instagram_content
from source_services.pinterest import download_profile_pinterest_content, download_search_pinterest_content
from source_services.twitter import download_profile_twitter_content, download_tag_twitter_content


def get_youtube_data(datatype: str, identifier: str, max_count: int):
    if datatype == 'channel':
        download_youtube_channel_content(identifier, max_count, datatype)
    if datatype == 'video':
        download_youtube_video_content(identifier, datatype)


def get_instagram_data(datatype: str, identifier: str, max_count: int):
    if datatype == 'profile':
        download_profile_instagram_content(identifier, max_count, datatype)
    if datatype == 'hashtag':
        download_tag_instagram_content(identifier, max_count, datatype)


def get_pinterest_data(datatype: str, identifier: str, max_count: int):
    if datatype == 'search_pins':
        download_search_pinterest_content(identifier, max_count, datatype)
    if datatype == 'profile':
        download_profile_pinterest_content(identifier, max_count, datatype)


def get_twitter_data(datatype: str, identifier: str, max_count: int):
    if datatype == 'profile':
        download_profile_twitter_content(identifier, max_count, datatype)
    if datatype == 'tag':
        download_tag_twitter_content(identifier, max_count, datatype)


def run(**kwargs):
    source = kwargs.get('source')
    datatype = kwargs.get('datatype')
    identifier = kwargs.get('identifier')
    max_count = int(kwargs.get('max_count'))

    if max_count < 1:
        raise Exception('Wrong value for -max--count. -max--count must be greater than 0.')

    if source not in 'instagram, pinterest, youtube, twitter':
        raise Exception('Wrong source. Valid source list: instagram, pinterest, youtube')

    if source == 'youtube':
        get_youtube_data(datatype, identifier, max_count)

    if source == 'instagram':
        get_instagram_data(datatype, identifier, max_count)

    if source == 'pinterest':
        get_pinterest_data(datatype, identifier, max_count)

    if source == 'twitter':
        get_twitter_data(datatype, identifier, max_count)


if __name__ == '__main__':
    """
    How to use the script:
        # python3 social_scrapper.py <source> <datatype> -i <identifier> --max-count <number>

    Examples:

    Get YouTube videos data for specific channel ID:
        # python3 social_scrapper.py youtube channel -i <channel_id> --max-count 100

    Get YouTube data for specific video:
        # python3 social_scrapper.py youtube video -i <video_id>
    """

    parser = argparse.ArgumentParser()
    parser.add_argument('source', help='The social media source. '
                                       'Option list: instagram, pinterest, youtube')
    parser.add_argument('datatype', help='The type of data you want to download.'
                                         'YouTube: channel, video. '
                                         'Instagram: profile, tag')
    parser.add_argument('-i', help='Content identifier: e.g. channel ID or video ID for YouTube')
    parser.add_argument('--max-count', help='Max number of downloaded content')
    args = parser.parse_args()

    run(source=args.source, datatype=args.datatype, identifier=args.i, max_count=args.max_count or 1)

